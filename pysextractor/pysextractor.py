"""Module to use sextractor through a python wrapper.

Author: Nicolas Gruel
Copyright: Copyright 2007 Nicolas Gruel
License: GPL v3 license.

10-02-2010:
    update for sextractor 2.8.6
    seems that the vector in the parameters have all been removed
    (see default parameter file created by sextractor itself)

23-02-2010:
    change read/write config and param method.

Historic: 

This wrapper has been inspired by another wrapper I saw some years
ago. I don't remember where and who was the author. I rewrite something 
similar.
"""
import os
import sys
import re
import subprocess

import convolution
import nnw

#-----------------------------------------
# from matplotlib and cookbook 
# Copyright: John Hunter for matplotlib

def is_string_like(obj):
    """Function to verify that the input parameter obj is a sort of string.

    Parameters
    ----------
    obj : string
       an object to test

    Return
    ------
    a boolean
    """
    # this is a workaround for a bug in numeric<23.1
    if hasattr(obj, 'shape'):
        return 0
    try:
        obj + ''
    except (TypeError, ValueError):
        return 0
    return 1

def to_filehandle(fname, flag='r'):
    """
    fname can be a filename or a file handle.  Support for gzipped
    files is automatic, if the filename ends in .gz.  flag is a
    read/write flag for file
    """
    if is_string_like(fname):
        if fname.endswith('.gz'):
            import gzip
            fileh = gzip.open(fname, flag)
        else:
            fileh = file(fname, flag)
    elif hasattr(fname, 'seek'):
        fileh = fname
    else:
        raise ValueError('fname must be a string or file handle')
    return fileh
#------------------------------------------

class Sextractor(object):
    """ Class to use SExtractor.

    Attributes
    ----------
    
    

    """
    def __init__(self, configfile='default.sex', catfile='default.cat',
                 paramfile='default.param', convfile='default.conv',
                 nnwfile='default.nnw', program='sex'):

        self.configfile = configfile
        self.paramfile = paramfile
        self.convfile = convfile
        self.nnwfile = nnwfile
        self.catfile = catfile
        self.program = program

        self.executable = ['sex', 'sextractor']
        self.param = None
        self.config = None

        self.convolution = convolution._convolution['default']['3x3']
        self.nnw = nnw._nnw

    def writeconv(self):
        """Function to save the convolution method
        
        See the _convolution dictionnary to know which one are available.
        Default one: default, 3x3
        """
        conv = self.convolution
        fileh = open(self.convfile, 'w')
        fileh.write('CONV NORM\n')
        fileh.write('# ' + conv['comment'] + '\n')
        for line in conv['kernel']:
            for value in line:
                fileh.write(str(value) + '  ')
            fileh.write('\n')
        fileh.close()

    def writennw(self):
        """Function to save the file with the neuronal network
        """
        fileh = open(self.nnwfile, 'w')
        fileh.write(self.nnw)
        fileh.close()

    def write(self):
        """Function which are creating all the files needed to use sextractor
        
        Examples
        --------
        >>> import pysextractor
        >>> sex = pysextractor.Sextractor()
        >>> sex.write()
        """
        self.writeconv()
        self.writennw()
        self.writeconfig()
        self.writeparam()

    def createconfig(self, filename=None):
        """Create a default config file from the sextractor executable.
                
        Parameters
        ----------     
        config : string, optional
           by default default.sex
        
        Returns
        -------
        self.config : dict
            update the variable self.config with the default config file.
        
        Examples
        --------
        >>> import pysextractor
        >>> sex = pysextractor.Sextractor()
        >>> sex.createconfig()
        """
        sextractor = subprocess.Popen('sex -dd', stderr=subprocess.PIPE,
                                      stdout=subprocess.PIPE, shell=True)
        stderr = sextractor.stderr.read()
        stdout = sextractor.stdout.read()
        if stderr != '':
            print 'sextractor failed with an error: %s' % stderr
            return

        if filename is not None:
            self.configfile = filename

        #stdout = re.sub('#','',stdout)
        fileh = open(self.configfile, 'w')
        fileh.write(stdout)
        fileh.close()
        self.getconfig()

    def getconfig(self, filename=None):
        """Read a config file.
        
        Parameters
        ----------
        filename : string, optional
           by default default.sex
           
        Return
        ------
        Update the self.config parameter with the value of the file.

        Examples
        --------
        >>> import pysextractor
        >>> sex = pysextractor.Sextractor()
        >>> sex.createconfig()
        >>> sex.getconfig()
        """

        if filename is not None:
            self.configfile = filename

        fileh = to_filehandle(self.configfile)
        config = {}
        config['keys'] = []
        comments = {}
        for nline, line in enumerate(fileh):
            _tmp1 = line.partition('#')
            if _tmp1[1] != '#':
                comments[nline] = line
                continue

            if not len(_tmp1[0]):
                comments[nline] = line
                continue

            if len(_tmp1[0].strip()):
                _tmp2 = _tmp1[0].split(None, 1)
                if len(_tmp2) != 2:
                    _tmp2.append('')
                _param = _tmp2[0].strip()
                config['keys'].append(_param)
                config[_param] = {}
                config[_param]['value'] = _tmp2[1].strip()
                config[_param]['comment'] = _tmp1[2].strip()
            else:
                config[_param]['comment'] += '\n'
                config[_param]['comment'] += line.rstrip()

        config['comments'] = comments
        if config['CATALOG_TYPE']['value'] != 'ASCII_HEAD':
            print 'BE CAREFUL: The catalog file %s is not an ' + \
                'ASCII sextractor file' % config['CATALOG_NAME']['value']
            print 'Rerun sextractor with the value "CATALOG_TYPE" ' + \
                'as "ASCII_HEAD" in the config file if needed'

        self.catfile = config['CATALOG_NAME']['value']
        self.paramfile = config['PARAMETERS_NAME']['value']
        self.convfile = config['FILTER_NAME']['value']
        self.nnwfile = config['STARNNW_NAME']['value']

        self.config = config

    def writeconfig(self, filename=None):
        """
        function to save the config (.sex) file
        
        Parameters
        ----------
        filename : string, optional
        
        Examples
        --------
        >>> import pysextractor
        >>> sex = pysextractor.Sextractor()
        >>> sex.createconfig()
        >>> sex.writeconfig()        
        """

        if filename is not None:
            self.configfile = filename

        if self.config is None:
            try:
                self.getconfig()
            except IOError:
                print "No configuration define and no config file found."
                return

        fileh = open(self.configfile, 'w')
        for key in self.config['keys']:
            fileh.write('%-17s%-15s# %s\n' % (key, self.config[key]['value'],
                                          self.config[key]['comment']))
        fileh.close()

        fileh = open(self.configfile, 'r')
        _tmp = []
        for line in fileh:
            _tmp.append(line)
        fileh.close()

        for i in self.config['comments']:
            _tmp.insert(i, self.config['comments'][i])

        fileh = open(self.configfile, 'w')
        for line in _tmp:
            fileh.write(line)
        fileh.close()

    def createparam(self, filename=None):
        """Function to create a default parameter file.
        
        This function is just a wrapper to the sextractor command -dp
        which dump a full list of measurement parameters 
        
        Parameters
        ----------        
        filename : string, optional
        
        Returns
        -------        
        self.param : dict
            update self.param with the default value.
            
        Note
        ----
        All parameters are commented, first character is #.
        
        Examples
        --------
        >>> import pysextractor
        >>> sex = pysextractor.Sextractor()
        >>> sex.createparam()     
        """
        sextractor = subprocess.Popen('sex -dp', stderr=subprocess.PIPE,
                                      stdout=subprocess.PIPE, shell=True)
        stderr = sextractor.stderr.read()
        stdout = sextractor.stdout.read()

        if stderr != '':
            print 'sextractor failed with an error: %s' % stderr
            return

        if filename is not None:
            self.paramfile = filename

        #stdout = re.sub('#','',stdout)
        fileh = open(self.paramfile, 'w')
        fileh.write(stdout)
        fileh.close()

        self.getparam()

    def getparam(self, filename=None, **kargs):
        """Function to read a sextractor parameter file.
        
        Parameters
        ----------
        filename : string, optional
            default default.param
            
        Return
        ------
        self.param : dict, 
            Update the self.param parameter with the value of the file.
            
        Examples
        --------
        >>> import pysextractor
        >>> sex = pysextractor.Sextractor()
        >>> sex.createparam()
        >>> sex.writeparam()
        >>> sex.getparam()
        """
        if 'kargs' in kargs:
            kargs.update(kargs)
            kargs.__delitem__('kargs')

        if filename is not None:
            self.paramfile = filename

        if 'configfile' in kargs and kargs['configfile']:
            self.getconfig(kargs['configfile'])
            if filename is not None and self.paramfile != filename:
                print 'The catalog file from the input and from the sex ' + \
                    'file are different'
                return

        if self.paramfile is None:
            print 'The param file name has not been define'
            return

        fileh = to_filehandle(self.paramfile)
        param = {}
        param['keys'] = []
        for line in fileh:
            if not len(line):
                continue

            try:
                index1 = line.index('[')
                index2 = line.index(']')
                unit = line[index1:index2 + 1]
                line = line[:index1].strip().split(None, 1)
                param['keys'].append(line[0][1:])
                param[line[0][1:]] = {'comments' : line[1],
                                      'unit' : unit, 'use' : False}
            except ValueError:
                line = line.strip().split(None, 1)
                #remove the default comment character #
                param['keys'].append(line[0][1:])
                param[line[0][1:]] = {'comments' : line[1],
                                      'unit': '', 'use' : False}

        self.param = param

    def writeparam(self, filename=None):
        """function to save the SExtractor parameter.
        
        Parameters
        ----------
        filename : string, optional
        
        Examples
        --------
        >>> sex = pysextractor.Sextractor()
        >>> sex.createparam()
        >>> sex.writeparam()
        """

        if filename is not None:
            self.paramfile = filename

        if self.param is None:
            try:
                self.getparam()
            except IOError:
                print "No parameters define and no parameter file found."
                return

        fileh = open(self.paramfile, 'w')
        for param in self.param['keys']:
            if not param in self.param.keys():
                continue
            if self.param[param]['use']:
                fileh.write('%-23s%-59s%-20s\n' % (param,
                                                 self.param[param]['comments'],
                                                 self.param[param]['unit']))
            else:
                fileh.write('#%-23s%-59s%-20s\n' % (param,
                                                  self.param[param]['comments'],
                                                  self.param[param]['unit']))
        fileh.close()

    def sexversion(self, prog=None, verbose=False):
        """Function returning the version of the sextractor 
        executable present on the system.
        
        Parameters
        ----------     
        prog : string, optional
            name of the version you want to test
        verbose : booleen, optional
        
        Return
        ------
        directory with the name of the executable as key and the version as \
        value
        """

        executable = self.executable
        if prog is not None:
            executable.append(prog)

        sexprog = {}
        for prog in executable:
            #do not test two times the same executable
            if prog in sexprog:
                continue
            sextractor = subprocess.Popen(prog, stderr=subprocess.PIPE,
                                          stdout=subprocess.PIPE, shell=True)
            stdout_err = sextractor.stdout.read() + sextractor.stderr.read()

            if (stdout_err.find('not found') == -1 and
                stdout_err.find('SExtractor') != -1):
                stdout_err = re.sub('\n', '', stdout_err).split()
                sexprog[prog] = stdout_err[stdout_err.index('version') + 1]
            else:
                if verbose:
                    if stdout_err.find('not found') != -1:
                        print 'The executable %s is not found' % prog
                    elif stdout_err.find('SExtractor') == -1:
                        print 'The executable %s is not a sextractor program' % \
                            prog

        if sexprog == {}:
            print 'No sextractor executable found. More information with ' + \
                'the option verbose'

        return sexprog

    def run(self, fitsfiles=None, **kwargs):
        #TODO: use argparse
        """Function to run sextractor.
        
        Parameters
        ----------
        
        fitsfiles : string
            name of the fitsfile on which you want to use sextractor 
            two files can be given (separate by a coma) see sextractor 
            documentation for more information.
        
        kwargs : dict
        
            kwargs arguments :
            
            prog : string
                sextractor executable
            configfile : string
                config sextractor file
            keywords : string
                -<keyword> <value> where keyword is the name of the keyword 
                you want to modify and value the value. 
                if present, that will update the <keyword> in the config 
                dictionnary or in the configfile if given.
            savekeys : boolean
                if yes that will save the keywords as define by the precedent
                parameter in the config file.
        """

        if fitsfiles is None:
            print 'The fits file %s is missing or no readable' % fitsfile
            return
        elif ',' in fitsfiles:
            tmp = fitsfiles.split(',')
            if not os.access(tmp[0], os.F_OK):
                print 'The fits file %s is missing or no readable' % tmp[0]
                return
            if not os.access(tmp[1], os.F_OK):
                print 'The fits file %s is missing or no readable' % tmp[1]
                return

        kwargs['fitsfile'] = fitsfiles

        if 'prog' not in kwargs:
            kwargs['prog'] = self.program

        if 'keywords' in kwargs:
            tmp1_keys = kwargs['keywords'].split('-')
            for tmp2_key in tmp1_keys:
                tmp3_key = tmp2_key.split()
                if len(tmp3_key):
                    self.config['keys'].append(tmp3_key[0])
                    self.config[tmp3_key[0]]['value'] = tmp3_key[1]
                    self.config[tmp3_key[0]]['comment'] = ''

        if 'savekeys' in kwargs:
            if 'configfile' in kwargs:
                self.configfile = kwargs['configfile']
            self.writeconfig(self.configfile)

        #test si prog existe    
        sexprog = self.sexversion(kwargs['prog'])
        if sexprog == {}:
            return

        if not kwargs['prog'] in sexprog:
            version = sexprog.values()
            #recupere les valeurs du dico ie les versions dispos de sextractor
            version.sort()
            version = version[-1]
            for key in sexprog.iterkeys():
                if sexprog[key] == version:
                    kwargs['prog'] = key
                else:
                    continue
        else:
            version = sexprog[kwargs['prog']]

        if 'configfile' not in kwargs or kwargs['configfile'] is None:
            kwargs['configfile'] = self.configfile

        if 'keywords' in kwargs and 'savekeys' not in kwargs:
            cmd = '%(prog)s %(fitsfile)s %(keywords)s' % kwargs
        else:
            cmd = '%(prog)s %(fitsfile)s -c %(configfile)s' % kwargs


        if 'verbose' in kwargs and kwargs['verbose'] != False:
            print 'sextractor version %s use with the command: %s' % \
                (version, cmd)

        #(stdin,stdout,stderr) = os.popen3(cmd)
        sextractor = subprocess.Popen(cmd, stderr=subprocess.PIPE,
                                      stdout=subprocess.PIPE, shell=True)
        stderr = sextractor.stderr.read()
        if stderr != '':
            print 'sextractor failed with an error: %s' % stderr
        return kwargs['prog'], version

    def test(self, fitsfiles):
        """Small function to do some stupid test. 
        Should be a unit test things...
        """
        self.paramfile = 'default.param'
        self.catfile = 'default.cat'
        self.configfile = 'default.sex'

        self.createparam()
        self.createconfig()
        param_use = ['NUMBER', 'FLUX_AUTO', 'MAG_AUTO']
        for key in param_use:
            self.param[key]['use'] = True
        #cree les fichiers necessaire par sextractor avec les param par default
        self.write()
        #fait tourner sextractor a travers python
        self.run(fitsfiles, verbose=True)
        #test if the keywords argument is working
        self.run(fitsfiles, keywords='-DETECT_THRESH 2', savekeys=True, verbose=True)

if __name__ == '__main__':
    fitsfile = sys.argv[1]
    sex = Sextractor()
    sex.test(fitsfile)
