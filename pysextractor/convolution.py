_convolution = {#'type' : ['default','gauss','mexhat','tophat','block'],
    'default' : {'characteristic' : 'a small pyramidal function (fast)',
                 '3x3' : {'kernel' : [[1, 2, 1],
                                     [2, 4, 2],
                                     [1, 2, 1]],
                          'comment' : ('3x3 "all-ground" convolution '
                                       'mask with FWHM = 2 pixels.')},
                 },
    'gauss' : {'characteristic' : ('a set of gaussian functions, '
                                   'for seeing FWHMs between 1.5 and 5'
                                   ' pixels (best for faint object '
                                   'detection),'),
               '1.5_3x3' : {'kernel' : [[0.109853, 0.300700, 0.109853],
                                         [0.300700, 0.823102, 0.300700],
                                         [0.109853, 0.300700, 0.109853]],
                             'comment' : ('3x3 convolution mask of a gaussian'
                                          'PSF with FWHM = 1.5 pixels.')
                             },
                '2.0_3x3' : {'kernel' : [[0.260856, 0.483068, 0.260856],
                                         [0.483068, 0.894573, 0.483068],
                                         [0.260856, 0.483068, 0.260856]],
                             'comment' : ('3x3 convolution mask of a gaussian'
                                          ' PSF with FWHM = 2.0 pixels.')
                             },
                '2.0_5x5' : {'kernel' : \
                                 [[0.006319, 0.040599, 0.075183, 0.040599,
                                   0.006319],
                                  [0.040599, 0.260856, 0.483068, 0.260856,
                                   0.040599],
                                  [0.075183, 0.483068, 0.894573, 0.483068,
                                   0.075183],
                                  [0.040599, 0.260856, 0.483068, 0.260856,
                                   0.040599],
                                  [0.006319, 0.040599, 0.075183, 0.040599,
                                   0.006319]],
                             'comment' : ('5x5 convolution mask of a gaussian'
                                          ' PSF with FWHM = 2.0 pixels.')
                             },
                '3.5_5x5' : {'kernel' : [[0.034673, 0.119131, 0.179633,
                                          0.119131, 0.034673],
                                         [0.119131, 0.409323, 0.617200,
                                          0.409323, 0.119131],
                                         [0.179633, 0.617200, 0.930649,
                                          0.617200, 0.179633],
                                         [0.119131, 0.409323, 0.617200,
                                          0.409323, 0.119131],
                                         [0.034673, 0.119131, 0.179633,
                                          0.119131, 0.034673]],
                             'comment' : ('5x5 convolution mask of a gaussian'
                                          ' PSF with FWHM = 2.5 pixels.')
                             },
                '3.0_5x5' : {'kernel' : [[0.092163, 0.221178, 0.296069,
                                          0.221178, 0.092163],
                                         [0.221178, 0.530797, 0.710525,
                                          0.530797, 0.221178],
                                         [0.296069, 0.710525, 0.951108,
                                          0.710525, 0.296069],
                                         [0.221178, 0.530797, 0.710525,
                                          0.530797, 0.221178],
                                         [0.092163, 0.221178, 0.296069,
                                          0.221178, 0.092163]],
                             'comment' : ('5x5 convolution mask of a gaussian'
                                          ' PSF with FWHM = 3.0 pixels.')
                             },
                '3.0_7x7' : {'kernel' : [[0.004963, 0.021388, 0.051328,
                                          0.068707, 0.051328, 0.021388,
                                          0.004963],
                                         [0.021388, 0.092163, 0.221178,
                                          0.296069, 0.221178, 0.092163,
                                          0.021388],
                                         [0.051328, 0.221178, 0.530797,
                                          0.710525, 0.530797, 0.221178,
                                          0.051328],
                                         [0.068707, 0.296069, 0.710525,
                                          0.951108, 0.710525, 0.296069,
                                          0.068707],
                                         [0.051328, 0.221178, 0.530797,
                                          0.710525, 0.530797, 0.221178,
                                          0.051328],
                                         [0.021388, 0.092163, 0.221178,
                                          0.296069, 0.221178, 0.092163,
                                          0.021388],
                                         [0.004963, 0.021388, 0.051328,
                                          0.068707, 0.051328, 0.021388,
                                          0.004963]],
                             'comment' : ('7x7 convolution mask of a gaussian'
                                          ' PSF with FWHM = 3.0 pixels.')
                             },
                '4.0_7x7' : {'kernel' : [[0.047454, 0.109799, 0.181612,
                                          0.214776, 0.181612, 0.109799,
                                          0.047454],
                                         [0.109799, 0.254053, 0.420215,
                                          0.496950, 0.420215, 0.254053,
                                          0.109799],
                                         [0.181612, 0.420215, 0.695055,
                                          0.821978, 0.695055, 0.420215,
                                          0.181612],
                                         [0.214776, 0.496950, 0.821978,
                                          0.972079, 0.821978, 0.496950,
                                          0.214776],
                                         [0.181612, 0.420215, 0.695055,
                                          0.821978, 0.695055, 0.420215,
                                          0.181612],
                                         [0.109799, 0.254053, 0.420215,
                                          0.496950, 0.420215, 0.254053,
                                          0.109799],
                                         [0.047454, 0.109799, 0.181612,
                                          0.214776, 0.181612, 0.109799,
                                          0.047454]],
                             'comment' : ('7x7 convolution mask of a gaussian'
                                          ' PSF with FWHM = 4.0 pixels.')
                             },
                '5.0_9x9' : {'kernel' : [[0.030531, 0.065238, 0.112208,
                                         0.155356, 0.173152, 0.155356,
                                         0.112208, 0.065238, 0.030531],
                                        [0.065238, 0.139399, 0.239763,
                                         0.331961, 0.369987, 0.331961,
                                         0.239763, 0.139399, 0.065238],
                                        [0.112208, 0.239763, 0.412386,
                                         0.570963, 0.636368, 0.570963,
                                         0.412386, 0.239763, 0.112208],
                                        [0.155356, 0.331961, 0.570963,
                                         0.790520, 0.881075, 0.790520,
                                         0.570963, 0.331961, 0.155356],
                                        [0.173152, 0.369987, 0.636368,
                                         0.881075, 0.982004, 0.881075,
                                         0.636368, 0.369987, 0.173152],
                                        [0.155356, 0.331961, 0.570963,
                                         0.790520, 0.881075, 0.790520,
                                         0.570963, 0.331961, 0.155356],
                                        [0.112208, 0.239763, 0.412386,
                                         0.570963, 0.636368, 0.570963,
                                         0.412386, 0.239763, 0.112208],
                                        [0.065238, 0.139399, 0.239763,
                                         0.331961, 0.369987, 0.331961,
                                         0.239763, 0.139399, 0.065238],
                                        [0.030531, 0.065238, 0.112208,
                                         0.155356, 0.173152, 0.155356,
                                         0.112208, 0.065238, 0.030531]],
                             'comment' : ('9x9 convolution mask of a gaussian'
                                          ' PSF with FWHM = 5.0 pixels.')
                             },
                },
    'mexhat' : {'characteristic' : ('"wavelets", producing a '
                                    'passband-filtering of the image, '
                                    'tuned to seeing FWHMs between 1.5 and'
                                    ' 5 pixels.\n'
                                    'Useful in very crowded star fields, '
                                    'or in the vicinity of a nebula.\n'
                                    'WARNING: may need a high THRESHOLD!!'),
                '1.5_5x5' : {'kernel' : [[-0.000109, -0.002374, -0.006302,
                                           - 0.002374, -0.000109],
                                         [-0.002374, -0.032222, -0.025569,
                                           - 0.032222, -0.002374],
                                         [-0.006302, -0.025569, 0.276021,
                                           - 0.025569, -0.006302],
                                         [-0.002374, -0.032222, -0.025569,
                                           - 0.032222, -0.002374],
                                         [-0.000109, -0.002374, -0.006302,
                                           - 0.002374, -0.000109]],
                             'comment' : ('5x5 convolution mask of a '
                                          'mexican-hat for images with '
                                          'FWHM~1.5 pixels.')
                             },
                '2.0_7x7' : {'kernel' : [[-0.000006, -0.000132, -0.000849,
                                           - 0.001569, -0.000849, -0.000132,
                                           - 0.000006],
                                         [-0.000132, -0.002989, -0.017229,
                                           - 0.028788, -0.017229, -0.002989,
                                           - 0.000132],
                                         [-0.000849, -0.017229, -0.042689,
                                           0.023455, -0.042689, -0.017229,
                                           - 0.000849],
                                         [-0.001569, -0.028788, 0.023455,
                                           0.356183, 0.023455, -0.028788,
                                           - 0.001569],
                                         [-0.000849, -0.017229, -0.042689,
                                           0.023455, -0.042689, -0.017229,
                                           - 0.000849],
                                         [-0.000132, -0.002989, -0.017229,
                                           - 0.028788, -0.017229, -0.002989,
                                           - 0.000132],
                                         [-0.000006, -0.000132, -0.000849,
                                           - 0.001569, -0.000849, -0.000132,
                                           - 0.000006]],
                             'comment' : ('7x7 convolution mask of a '
                                          'mexican-hat for images with '
                                          'FWHM~2.0 pixels.')
                             },
                '2.5_7x7' : {'kernel' : [[-0.000284, -0.002194, -0.007273,
                                          - 0.010722, -0.007273, -0.002194,
                                          - 0.000284],
                                         [-0.002194, -0.015640, -0.041259,
                                          - 0.050277, -0.041259, -0.015640,
                                          - 0.002194],
                                         [-0.007273, -0.041259, -0.016356,
                                          0.095837, -0.016356, -0.041259,
                                          - 0.007273],
                                         [-0.010722, -0.050277, 0.095837,
                                          0.402756, 0.095837, -0.050277,
                                          - 0.010722],
                                         [-0.007273, -0.041259, -0.016356,
                                          0.095837, -0.016356, -0.041259,
                                          - 0.007273],
                                         [-0.002194, -0.015640, -0.041259,
                                          - 0.050277, -0.041259, -0.015640,
                                          - 0.002194],
                                         [-0.000284, -0.002194, -0.007273,
                                          - 0.010722, -0.007273, -0.002194,
                                          - 0.000284]],
                             'comment' : ('7x7 convolution mask of a '
                                          'mexican-hat for images with '
                                          'FWHM~2.5 pixels.')
                             },
                '3.0_9x9' : {'kernel' : [[-0.000041, -0.000316, -0.001357,
                                          - 0.003226, -0.004294, -0.003226,
                                          - 0.001357, -0.000316, -0.000041],
                                         [-0.000316, -0.002428, -0.010013,
                                          - 0.022204, -0.028374, -0.022204,
                                          - 0.010013, -0.002428, -0.000316],
                                         [-0.001357, -0.010013, -0.035450,
                                          - 0.054426, -0.050313, -0.054426,
                                          - 0.035450, -0.010013, -0.001357],
                                         [-0.003226, -0.022204, -0.054426,
                                          0.033057, 0.164532, 0.033057,
                                          - 0.054426, -0.022204, -0.003226],
                                         [-0.004294, -0.028374, -0.050313,
                                          0.164532, 0.429860, 0.164532,
                                          - 0.050313, -0.028374, -0.004294],
                                         [-0.003226, -0.022204, -0.054426,
                                          0.033057, 0.164532, 0.033057,
                                          - 0.054426, -0.022204, -0.003226],
                                         [-0.001357, -0.010013, -0.035450,
                                          - 0.054426, -0.050313, -0.054426,
                                          - 0.035450, -0.010013, -0.001357],
                                         [-0.000316, -0.002428, -0.010013,
                                          - 0.022204, -0.028374, -0.022204,
                                          - 0.010013, -0.002428, -0.000316],
                                         [-0.000041, -0.000316, -0.001357,
                                          - 0.003226, -0.004294, -0.003226,
                                          - 0.001357, -0.000316, -0.000041]],
                             'comment' : ('9x9 convolution mask of a '
                                          'mexican-hat for images with '
                                          'FWHM~3.0 pixels.')
                             },
                '4.0_9x9' : {'kernel' : [[-0.002250, -0.007092, -0.015640,
                                          - 0.024467, -0.028187, -0.024467,
                                          - 0.015640, -0.007092, -0.002250],
                                         [-0.007092, -0.021141, -0.041403,
                                          - 0.054742, -0.057388, -0.054742,
                                          - 0.041403, -0.021141, -0.007092],
                                         [-0.015640, -0.041403, -0.057494,
                                          - 0.024939, 0.008058, -0.024939,
                                          - 0.057494, -0.041403, -0.015640],
                                         [-0.024467, -0.054742, -0.024939,
                                          0.145167, 0.271470, 0.145167,
                                          - 0.024939, -0.054742, -0.024467],
                                         [-0.028187, -0.057388, 0.008058,
                                          0.271470, 0.459236, 0.271470,
                                          0.008058, -0.057388, -0.028187],
                                         [-0.024467, -0.054742, -0.024939,
                                          0.145167, 0.271470, 0.145167,
                                          - 0.024939, -0.054742, -0.024467],
                                         [-0.015640, -0.041403, -0.057494,
                                          - 0.024939, 0.008058, -0.024939,
                                          - 0.057494, -0.041403, -0.015640],
                                         [-0.007092, -0.021141, -0.041403,
                                          - 0.054742, -0.057388, -0.054742,
                                          - 0.041403, -0.021141, -0.007092],
                                         [-0.002250, -0.007092, -0.015640,
                                          - 0.024467, -0.028187, -0.024467,
                                          - 0.015640, -0.007092, -0.002250]],
                             'comment' : ('9x9 convolution mask of a '
                                          'mexican-hat for images with '
                                          'FWHM~4.0 pixels.')
                             },
                '5.0_11x11' : {'kernel' : [[-0.002172, -0.005657, -0.011702,
                                            - 0.019279, -0.025644, -0.028106,
                                            - 0.025644, -0.019279, -0.011702,
                                            - 0.005657, -0.002172],
                                           [-0.005657, -0.014328, -0.028098,
                                            - 0.042680, -0.052065, -0.054833,
                                            - 0.052065, -0.042680, -0.028098,
                                            - 0.014328, -0.005657],
                                           [-0.011702, -0.028098, -0.049016,
                                            - 0.059439, -0.051288, -0.043047,
                                            - 0.051288, -0.059439, -0.049016,
                                            - 0.028098, -0.011702],
                                           [-0.019279, -0.042680, -0.059439,
                                            - 0.030431, 0.047481, 0.093729,
                                            0.047481, -0.030431, -0.059439,
                                            - 0.042680, -0.019279],
                                           [-0.025644, -0.052065, -0.051288,
                                            0.047481, 0.235153, 0.339248,
                                            0.235153, 0.047481, -0.051288,
                                            - 0.052065, -0.025644],
                                           [-0.028106, -0.054833, -0.043047,
                                            0.093729, 0.339248, 0.473518,
                                            0.339248, 0.093729, -0.043047,
                                            - 0.054833, -0.028106],
                                           [-0.025644, -0.052065, -0.051288,
                                            0.047481, 0.235153, 0.339248,
                                            0.235153, 0.047481, -0.051288,
                                            - 0.052065, -0.025644],
                                           [-0.019279, -0.042680, -0.059439,
                                            - 0.030431, 0.047481, 0.093729,
                                            0.047481, -0.030431, -0.059439,
                                            - 0.042680, -0.019279],
                                           [-0.011702, -0.028098, -0.049016,
                                            - 0.059439, -0.051288, -0.043047,
                                            - 0.051288, -0.059439, -0.049016,
                                            - 0.028098, -0.011702],
                                           [-0.005657, -0.014328, -0.028098,
                                            - 0.042680, -0.052065, -0.054833,
                                            - 0.052065, -0.042680, -0.028098,
                                            - 0.014328, -0.005657],
                                           [-0.002172, -0.005657, -0.011702,
                                            - 0.019279, -0.025644, -0.028106,
                                            - 0.025644, -0.019279, -0.011702,
                                            - 0.005657, -0.002172]],
                               'comment' : ('11x11 convolution mask of a '
                                            'mexican-hat for images with '
                                            'FWHM~5.0 pixels.')
                               },
                },
    'tophat' : {'characteristic' : ('a set of "top-hat" functions. '
                                    'Use them to detect extended, '
                                    'low-surface brightness objects, '
                                    'with a very low THRESHOLD.'),
                '1.5_3x3' : {'kernel' : [[0.000000, 0.180000, 0.000000],
                                         [0.180000, 1.000000, 0.180000],
                                         [0.000000, 0.180000, 0.000000]],
                             'comment' : ('3x3 convolution mask of a '
                                          'top-hat PSF with '
                                          'diameter = 1.5 pixels.')
                             },
                '2.0_3x3' : {'kernel' : [[0.080000, 0.460000, 0.080000],
                                         [0.460000, 1.000000, 0.460000],
                                         [0.080000, 0.460000, 0.080000]],
                             'comment' : ('3x3 convolution mask of a top-hat'
                                          ' PSF with diameter = 2.0 pixels.')
                             },
                '2.5_3x3' : {'kernel' : [[0.260000, 0.700000, 0.260000],
                                         [0.700000, 1.000000, 0.700000],
                                         [0.260000, 0.700000, 0.260000]],
                             'comment' : ('3x3 convolution mask of a top-hat'
                                          ' PSF with diameter = 2.5 pixels.')
                             },
                '3.0_3x3' : {'kernel' : [[0.560000, 0.980000, 0.560000],
                                         [0.980000, 1.000000, 0.980000],
                                         [0.560000, 0.980000, 0.560000]],
                             'comment' : ('3x3 convolution mask of a top-hat'
                                          ' PSF with diameter = 3.0 pixels.')
                             },
                '4.0_5x5' : {'kernel' : [[0.000000, 0.220000, 0.480000,
                                          0.220000, 0.000000],
                                         [0.220000, 0.990000, 1.000000,
                                          0.990000, 0.220000],
                                         [0.480000, 1.000000, 1.000000,
                                          1.000000, 0.480000],
                                         [0.220000, 0.990000, 1.000000,
                                          0.990000, 0.220000],
                                         [0.000000, 0.220000, 0.480000,
                                          0.220000, 0.000000]],
                             'comment' : ('5x5 convolution mask of a top-hat'
                                          ' PSF with diameter = 4.0 pixels.')
                                         },
                '5.0_5x5' : {'kernel' : [[0.150000, 0.770000, 1.000000,
                                          0.770000, 0.150000],
                                         [0.770000, 1.000000, 1.000000,
                                          1.000000, 0.770000],
                                         [1.000000, 1.000000, 1.000000,
                                          1.000000, 1.000000],
                                         [0.770000, 1.000000, 1.000000,
                                          1.000000, 0.770000],
                                         [0.150000, 0.770000, 1.000000,
                                          0.770000, 0.150000]],
                             'comment' : ('5x5 convolution mask of a top-hat'
                                          'PSF with diameter = 5.0 pixels.')
                             },
                },
    'block' : {'characteristic' : ('a small "block" function '
                                   '(for rebinned images like those of'
                                   ' the DeNIS survey).'),
               '3x3' : {'kernel' : [[1, 1, 1],
                                    [1, 1, 1],
                                    [1, 1, 1]],
                        'comment' : ('3x3 convolution mask of a block-function'
                                     ' PSF (ex: DeNIS PSF).')}
                           },
               }
