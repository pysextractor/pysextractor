import os
from distribute_setup import use_setuptools
use_setuptools()

from setuptools import setup, find_packages

version = '0.1'
README = os.path.join(os.path.dirname(__file__), 'README')
long_description = open(README).read() + '\n\n'
setup(name='pysextractor',
      version=version,
      description=("A python wrapper for Sextractor software"),
      long_description=long_description,
      classifiers=[
        "Programming Language :: Python",
        ],
      keywords='astronomy science',
      url='https://gitorious.org/pysextractor',
      author='Nicolas Gruel',
      author_email='nicolas.gruel@gmail.com',
      license='GPLv3',
      packages=find_packages(exclude='tests'),
      packages_data={'': ['*.dat', '*.txt', '*.rst']},

      install_requires=['distribute']
     )
